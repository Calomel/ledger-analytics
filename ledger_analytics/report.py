"""
Ledger Summary Display
"""
import argparse, subprocess, unittest
from dataclasses import dataclass
from datetime import datetime
import pandas, pygtrie
import numpy as N

import bokeh
import bokeh.palettes
import bokeh.plotting
import bokeh.transform
import bokeh.layouts

### Financial Data Handling ###

### Tag and CSV Handling ###

TAG_SPLITTER = ";"

def tags_split(s: str) -> list[str]:

    """
    Handle the "A:aa B:bb" case.
    """

    # Position of a starting char + colon
    tagCandidates = []
    limit = 0
    for i,c in enumerate(s):

        if c == ':':
            # Scan back to the last space
            for j in reversed(range(limit,i)):
                if s[j] == ' ' and s[j+1].isupper():
                    tagCandidates.append(j+1)
                    break
                if j == 0 and s[0].isupper():
                    tagCandidates.append(0)
                    break
            limit = i

    nCandidates = len(tagCandidates)
    tagCandidates.append(len(s))
    return  [s[tagCandidates[i]:tagCandidates[i+1]].strip()
            for i in range(nCandidates)]
def tags_extract(comment: str) -> list[str]:
    comment = str(comment)
    li = [tag
          for x in comment.replace(';',"\n").split("\n")
          for tag in tags_split(x)]
    return li
def tags_transform_it(li: list[str]) -> list[str]:
    """
    Extract tags from comments. Some heuristics had to be used because the
    sloppy way ledger prints its csv.

    If two tags get sent into one line e.g. "A: aa B: bb", the heuristic
    locates :s and split them into individual tags.

    If one tag is a predecessor of another, the tags are merged.
    """

    # Has to be a reusable type
    liTags = list(map(tags_extract, li))
    rawTags = { t for li in liTags for t in li }
    rawTags = sorted(rawTags, key=len)

    tagRemap = {}

    trie = pygtrie.CharTrie()
    for tag in rawTags:
        prefix = trie.longest_prefix(tag)
        if prefix.value:
            # Tag already registered
            tagRemap[tag] = prefix.key
        else:
            # Tag has not been registered
            trie[tag] = True

    return [TAG_SPLITTER.join(tagRemap.get(x, x).strip() for x in row)
            for row in liTags]

def tags_get(tags: str) -> list[str]:
    if len(tags) == 0:
        return []
    li = tags.split(TAG_SPLITTER)
    return li
def has_tags(tags: list[str]) -> bool:
    # Exclude payee tags
    tags = [t for t in tags_get(tags)
            if not t.startswith('Payee:')]
    return len(tags) > 0
def account_is_io(account: str) -> bool:
    return account.startswith('Expenses:') \
        or account.startswith('Income:')


KEY_DATE = 'Date'
KEY_UNBAL = 'Unbalanced'
KEY_TNAME = 'Transaction Name'
KEY_ACCOUNT = 'Account'
KEY_CURRENCY = 'Currency'
KEY_AMOUNT = 'Amount'
KEY_ANNOT = 'Annotation' # Whether a name begins with star
KEY_COMMENT = 'Comment'

# Custom tag
KEY_TAGS = 'Tags'
KEY_VALUE = 'Value' # Unified Value in some unit
KEY_MONTH = 'Month'
KEY_CAT1 = 'Cat1'
KEY_CAT2 = 'Cat2'
# Combined category

KEYS = [
    (KEY_DATE, str),
    (KEY_UNBAL, str),
    (KEY_TNAME, str),
    (KEY_ACCOUNT, str),
    (KEY_CURRENCY, str),
    (KEY_AMOUNT, float),
    (KEY_ANNOT, str),
    (KEY_COMMENT, str),
]
def ledger_load(path='now.ledger') -> pandas.DataFrame:
    pipe = subprocess.Popen(["ledger", "csv", "-f", path],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    df = pandas.read_csv(pipe.stdout,
                         names = [k for k,_ in KEYS],
                         dtype = object,
                         parse_dates=[KEY_DATE])
    df[KEY_TAGS] = tags_transform_it(df[KEY_COMMENT])
    df[KEY_DATE] = pandas.to_datetime(df[KEY_DATE], format='%Y/%m/%d')
    df[KEY_AMOUNT] = df[KEY_AMOUNT].astype(float)
    return df
def ledger_unify_currency(df: pandas.DataFrame) -> pandas.DataFrame:
    RATIO = {
        'US$': 1.00,
        'CA$': 0.73,
    }
    def row_func(args):
        amount = args[KEY_AMOUNT]
        curr = args[KEY_CURRENCY]
        return amount * RATIO[curr]
    df[KEY_VALUE] = df.apply(row_func, axis=1)
    return df
def ledger_monthly_expense_summary(df: pandas.DataFrame,
                                   invert_incomes=True) -> pandas.DataFrame:
    # First eliminate all transactions with the trip tag
    mask_unbal = df[KEY_UNBAL].isnull()
    mask_trip = df[KEY_TAGS].map(has_tags)
    # Count expenses and incomes only
    mask_expense_income = df[KEY_ACCOUNT].map(account_is_io)
    df = df[mask_expense_income & ~mask_trip & mask_unbal]
    df = ledger_unify_currency(df)


    def to_month(x):
        d = x[KEY_DATE]
        return f"{d.year}-{d.month:02d}"
    def to_secondary_account(x):
        a = x.split(':')
        if len(a) == 1:
            return a[0], "Unspecified"
        elif len(a) == 2:
            return a
        else:
            return a[0], a[1]
    df[KEY_MONTH] = df.apply(to_month, axis=1)
    seriesTC = df[KEY_ACCOUNT].apply(to_secondary_account)
    df[KEY_CAT1] = seriesTC.apply(lambda x:x[0])
    df[KEY_CAT2] = seriesTC.apply(lambda x:x[1])

    df = df[[KEY_MONTH, KEY_CAT1, KEY_CAT2, KEY_VALUE]]\
        .groupby([KEY_MONTH, KEY_CAT1, KEY_CAT2], as_index=False)\
        .sum()

    # Invert the incomes
    if invert_incomes:
        mask_income = df[KEY_CAT1] == 'Income'
        df[KEY_VALUE][mask_income] = -df[KEY_VALUE][mask_income]

    return df


### Plotting Functions ###

@dataclass(init=True)
class PlottingParams:

    seriesWidth: int = 750
    seriesHeight: int = 750
    seriesBarWidth: float = 0.9
    proportionWidth: int = seriesWidth
    proportionRadius: float = 0.6
    valueFormat: str = '{0.2f}'



def plot_monthly(df: pandas.DataFrame,
                 plottingParams: PlottingParams) -> bokeh.plotting.figure:
    """
    df: Main df
    """
    df = ledger_monthly_expense_summary(df, invert_incomes=False)

    KEY_COMB = "Category"
    df[KEY_COMB] = df[KEY_CAT1] + ':' + df[KEY_CAT2]

    mask_expense = df[KEY_CAT1] == 'Expenses'
    mask_income = df[KEY_CAT1] == 'Income'

    keys_month = sorted(list(set(df[KEY_MONTH])))
    keys_expense_categories = list(sorted(set(df[mask_expense][KEY_COMB])))
    keys_income_categories = list(sorted(set(df[mask_income][KEY_COMB])))
    tooltips = f"$name @$name{plottingParams.valueFormat}"
    p = bokeh.plotting.figure(x_range=keys_month,
                              height=plottingParams.seriesHeight,
                              width=plottingParams.seriesWidth,
                              title='Expenses and Incomes',
                              toolbar_location=None,
                              tools='hover',
                              tooltips=tooltips)

    df_expenses = df[mask_expense]\
        .pivot(index=KEY_MONTH, columns=KEY_COMB, values=KEY_VALUE) \
        .fillna(0.0) \
        .reset_index()
    palette_expenses=bokeh.palettes.brewer['Blues'][len(keys_expense_categories)]
    p.vbar_stack(keys_expense_categories,
                 x=KEY_MONTH, width=0.9,
                 color=palette_expenses,
                 source=df_expenses,
                 legend_label=keys_expense_categories)

    df_income = df[mask_income]\
        .pivot(index=KEY_MONTH, columns=KEY_COMB, values=KEY_VALUE) \
        .fillna(0.0) \
        .reset_index()
    palette_income=bokeh.palettes.brewer['Reds'][len(keys_income_categories)]
    p.vbar_stack(keys_income_categories,
                 source=df_income,
                 x=KEY_MONTH,
                 width=plottingParams.seriesBarWidth,
                 color=palette_income,
                 legend_label=keys_income_categories)


    # Combined differential
    df_total = df[[KEY_MONTH, KEY_VALUE]]\
        .groupby(KEY_MONTH).sum().reset_index()
    p.line(x=KEY_MONTH, y=KEY_VALUE,
           source=df_total,
           color='black', alpha=0.4, line_width=4,
           legend_label="Net")

    p.xaxis.axis_label = KEY_MONTH
    p.yaxis.axis_label = "Total (USD)"
    p.xgrid.grid_line_color = None
    p.axis.minor_tick_line_color = None
    p.outline_line_color = None
    p.legend.location = "bottom_left"

    return p

def plot_proportion(df: pandas.DataFrame,
                    plottingParams: PlottingParams) -> bokeh.plotting.figure:
    df = ledger_monthly_expense_summary(df, invert_incomes=False)

    # Default selection
    month = df[KEY_MONTH][len(df.index) // 2]
    prefix = 'Expenses'
    key_cat = KEY_CAT2

    # Generate small table
    df_plot = df[(df[KEY_MONTH] == month) & (df[KEY_CAT1] == prefix)] \
        [[KEY_VALUE, key_cat]]\
        .sort_values(by=key_cat) \
        .copy()
    df_plot['angle'] = df_plot[KEY_VALUE] / df_plot[KEY_VALUE].sum() * 2 * N.pi
    df_plot['color'] = bokeh.palettes.Light[len(df_plot.index)]


    title = f"{month} {prefix}"
    tooltips = f"@{key_cat}: @{KEY_VALUE}{plottingParams.valueFormat}"
    p = bokeh.plotting.figure(width=plottingParams.proportionWidth,
                              height=plottingParams.proportionWidth,
                              title=title,
                              toolbar_location=None,
                              tools="hover", tooltips=tooltips,
                              x_range=(-plottingParams.proportionRadius-.1, 1.0))

    p.wedge(x=0, y=1, radius=plottingParams.proportionRadius,
            start_angle=bokeh.transform.cumsum('angle', include_zero=True),
            end_angle=bokeh.transform.cumsum('angle'),
            line_color='white', fill_color='color',
            legend_field=key_cat, source=df_plot)

    p.axis.axis_label = None
    p.axis.visible = False
    p.grid.grid_line_color = None
    return p







### Script Entrypoints ###

def exec_summary(args):
    df = ledger_load()
    tagsAll = { t
                for x in df[KEY_TAGS]
                for t in tags_split(x)
               }
    print(list(tagsAll))
    df.to_csv('/tmp/test.csv', index=False)
def exec_monthly(args):
    df = ledger_load()
    df = ledger_monthly_expense_summary(df)
    print(df[df[KEY_CAT1] == 'Expenses'])
    print(df[df[KEY_CAT1] == 'Expenses'].pivot(index=KEY_MONTH, columns=KEY_CAT2, values=KEY_VALUE).fillna(0.0))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Ledger Report Generation')
    parser.add_argument('mode', type=str, default='summary',
                        help='Execution mode')

    args = parser.parse_args()
    if args.mode == 'summary':
        exec_summary(args)
    elif args.mode == 'monthly':
        exec_monthly(args)
    elif args.mode == 'test':
        unittest.main(argv=['first-arg-is-ignored'], exit=False)



elif __name__.startswith('bokeh_app_'):
    plottingParams = PlottingParams()

    df = ledger_load()
    p_monthly = plot_monthly(df, plottingParams)
    p_proportion = plot_proportion(df, plottingParams)
    row = bokeh.layouts.row(p_monthly, p_proportion)

    from bokeh.io import curdoc

    curdoc().add_root(row)
    curdoc().title = "Expenses and Incomes"




### Unit Tests ###

class TestReport(unittest.TestCase):
    def test_tags(self):
        self.assertEqual(tags_split('A:aa B:bb'), ['A:aa', 'B:bb'])
        self.assertEqual(tags_split('There are no tags here'), [])
        self.assertEqual(tags_split('A:b:c'), ['A:b:c'])
        self.assertEqual(tags_split('A:a 1 B:c:d 2'), ['A:a 1', 'B:c:d 2'])
        self.assertEqual(tags_split('  Some:Spaces Are Here    Hayo:B'),

                         ['Some:Spaces Are Here', "Hayo:B"])
        self.assertListEqual(tags_get(""), [])
        self.assertListEqual(tags_get("ABC"), ["ABC"])
        self.assertListEqual(tags_get("ABC;DEF"), ["ABC", "DEF"])
        self.assertListEqual(sorted(tags_extract(" A:1 B:2 \n C:3")),
                         ['A:1', 'B:2', 'C:3'])
        self.assertListEqual(tags_transform_it(
            ['A:a random A:b', 'A:a', 'A:b', 'A:b some B:c']),
            ["A:a;A:b", 'A:a', 'A:b', "A:b;B:c"])
